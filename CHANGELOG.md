# Changelog

## Unreleased (2021-06-30)

#### Fixes

* Add site directory to gitignore and remove site directory.
* Remove docs directory from gitignore.
* Fix CI configuration.
#### Others

* Take 2 of pages deploy.
