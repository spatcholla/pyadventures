# Overview

Python project starter.

This project was generated with [cookiecutter](https://github.com/audreyr/cookiecutter) using [spatcholla/py-project-template](https://gitlab.com/Spatcholla/py-project-template).

[![Coverage Status](https://img.shields.io/coveralls/spatcholla/pyadventures.svg)](https://coveralls.io/r/spatcholla/pyadventures)
[![Scrutinizer Code Quality](https://img.shields.io/scrutinizer/g/spatcholla/pyadventures.svg)](https://scrutinizer-ci.com/g/spatcholla/pyadventures)

## Setup

### Requirements

* Python 3.9+

### Installation

Install it directly into an activated virtual environment:

```shell
pip install PyAdventures
```

or add it to your [Poetry](https://poetry.eustace.io/) project:

```shell
poetry add PyAdventures
```

## Usage

After installation, the package can imported:

```shell
$ python
>>> import src
>>> src.__version__
```
